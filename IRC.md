# IRC

This Page is about the Indicproject IRC channel and basic irc etiquette  

## Channel Details

* Main Channel Name : **#indicproject**
* Server : **irc.freenode.net**
* Webchat link : https://webchat.freenode.net/?randomnick=0&channels=#indicproject
* Telegram IRC Bridge: https://t.me/indicproject

## Other Project specific IRC channels


* '''libindic''' :  #libindic on irc.oftc.net , [click here for webchat](https://webchat.oftc.net/?randomnick=0&channels=#libindic)
* '''Varnam''' : #varnamproject on irc.freenode.net [click here for webchat](https://webchat.freenode.net/?randomnick=0&channels=#varnamproject)

## IRC etiquette



* Please speak out in channel, don't send PM's to individuals without asking first
* Please don't ask to ask
* Don't speak in ALL CAPS - IT'S LIKE SHOUTING (!!!), 
* No Flooding - which is sending too much information to the channel at once. If you want to show us some code/logs use a pastebin (eg: https://paste.debian.net/) or etherpad (https://public.etherpad-mozilla.org/)  
* Behave as you would in a real life conversation
* Read the /topic



## Tips

* For detailed technical discussions, project specific channels will be better. Because, you will find more contributors in the project specific channels.  
* Check the timezone of the main developers, if they're not awake when you IRC them, you'll hardly get any reply. (Most of us are in  GMT + 5.30, ie Indian Standard Time, Our IRC peek time is 7.00 PM IST to 1.00 AM IST) 
* Be patient. If there is no activity, it usually means that no one has read what you wrote yet. If no one responds, they don't know or didn't notice. You can retry after a while, or ask if anyone has any clue with regards to your question x minutes ago. If worst comes to worst, ping the ops (the ones whose nicks starts with an @ / the ones with a green light near to their nick)  
* Do ask your questions. We strive to provide answers to the best of our abilities.
* Do mingle, make conversation, watch. IRC is intimidating at first, but with some practice, and good listening skills, you will be making new friends from around the globe in no time at all. :)
* [Read This](https://github.com/fizerkhan/irc-etiquette)


