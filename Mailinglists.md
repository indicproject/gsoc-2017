# GSOC specific participation

Join in GSOC 2017 Mailinglist and send queries to : <gsoc@indicproject.org> [[join List](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)]

These messages can be viewed at the [corresponding archive in Google Groups](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)

GSoC updates will be posted on this Discourse category https://discourse.indicproject.org/t/we-applied-for-gsoc-2017-follow-this-thread-for-updates/96

 - For Libindic  related discussion join in [SILPA-Discuss](https://lists.nongnu.org/mailman/listinfo/silpa-discuss) list
 - For Varnam project related ideas join in [Varnamproject-discuss](https://lists.nongnu.org/mailman/listinfo/varnamproject-discuss) list



# General project questions 

contact@indicproject.org