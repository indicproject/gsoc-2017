This repo houses the activities for GSOC 2017 for Indicproject.org.

 - [Projects](https://gitlab.com/indicproject/Projects)
 - [GSOC2017 Project Ideas List](https://gitlab.com/indicproject/gsoc-2017/blob/master/ideas-list.md)
 - [Guidelines](https://gitlab.com/indicproject/gsoc-2017/blob/master/Guidelines.md)
 - [IRC](https://gitlab.com/indicproject/gsoc-2017/blob/master/IRC.md)
 - [Mailinglists](https://gitlab.com/indicproject/gsoc-2017/blob/master/Mailinglists.md) 

# Contact

- IRC : [#indicproject on Freenode](irc://irc.freenode.net/indicproject)
- join : [<gsoc@indicproject.org>](https://groups.google.com/a/indicproject.org/forum/#!forum/gsoc)
- Enquiries  : <contact@indicproject.org>
- Discourse : http://discourse.indicproject.org/